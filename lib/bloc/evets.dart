import 'package:flutter_cube/flutter_cube.dart';

abstract class Event {}

class DropDiceFirst extends Event {
  int initialState;
  int reverseUp;
  bool isAudio;
  bool isVibrate;

  DropDiceFirst(
      {required this.initialState,
      required this.reverseUp,
      required this.isAudio,
      required this.isVibrate});
}

class DropDiceSecond extends Event {
  int initialState;
  int reverseUp;
  bool isAudio;
  bool isVibrate;

  DropDiceSecond(
      {required this.initialState,
      required this.reverseUp,
      required this.isAudio,
      required this.isVibrate});
}
