import 'package:flutter_cube/flutter_cube.dart';

abstract class DiceState {}

class FirstDiceState extends DiceState {
  Object dice;
  int randomNumber;
  int initialState;
  int reverseUp;
  bool isAudio;
  bool isVibrate;

  FirstDiceState(
      {required this.dice,
      required this.randomNumber,
      required this.initialState,
      required this.reverseUp,
      required this.isAudio,
      required this.isVibrate});
}

class SecondDiceState extends DiceState {
  int randomNuber;
  Object dice;
  int initialState;
  int reverseUp;
  bool isAudio;
  bool isVibrate;

  SecondDiceState(
      {required this.dice,
      required this.randomNuber,
      required this.initialState,
      required this.reverseUp,
      required this.isAudio,
      required this.isVibrate});
}

class ErrorState extends DiceState {}
