import 'dart:math';
import 'package:dice/api/dice_api.dart';
import 'package:dice/bloc/evets.dart';
import 'package:dice/bloc/states.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_cube/flutter_cube.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/services.dart';

class DiceBloc extends Bloc<Event, DiceState> {
  DiceBloc(DiceState initialState) : super(initialState);
  final random = new Random();
  final diceApi = new DiceApi();
  Object dice = new Object();
  Scene scene = new Scene();
  AudioCache audioCache = AudioCache();

  @override
  Stream<DiceState> mapEventToState(Event event) async* {
    await audioCache.load(diceApi.getSound());
    dice = diceApi.getCube();
    if (event is DropDiceFirst) {
      try {
        if(event.isVibrate)HapticFeedback.heavyImpact();
        if(event.isAudio)audioCache.play(diceApi.getSound(), volume: 4.0);
        yield FirstDiceState(
            dice: dice,
            randomNumber: random.nextInt(6),
            initialState: event.initialState,
            reverseUp: event.reverseUp,
            isVibrate: event.isVibrate,
            isAudio: event.isAudio);
      } catch (_) {
        print(_);
        yield ErrorState();
      }
    }

    if (event is DropDiceSecond) {
      try {
        if(event.isVibrate)HapticFeedback.heavyImpact();
        if(event.isAudio)audioCache.play(diceApi.getSound(), volume: 4.0);
        yield SecondDiceState(
            dice: dice,
            randomNuber: random.nextInt(6),
            initialState: event.initialState,
            reverseUp: event.reverseUp,
            isVibrate: event.isVibrate,
            isAudio: event.isAudio);
      } catch (_) {
        yield ErrorState();
      }
    }
  }
}
