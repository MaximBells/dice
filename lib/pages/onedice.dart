import 'dart:ui';

import 'package:dice/bloc/dice_bloc.dart';
import 'package:dice/bloc/evets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:dice/api/dice_api.dart';
import 'package:flutter_cube/flutter_cube.dart';

// ignore: must_be_immutable
class FirstDice extends StatelessWidget {
  Object dice;
  int randomNumber;
  bool endAnimation = false;
  int initialState;
  int reverseUp;

  FirstDice(
      {required this.dice,
      required this.randomNumber,
      required this.initialState,
      required this.reverseUp,
      required this.isAudio,
      required this.isVibrate});

  final diceApi = DiceApi();
  bool isAudio;
  bool isVibrate;
  bool showBottom = false;

  Color getColor(Set<MaterialState> states) {
    const Set<MaterialState> interactiveStates = <MaterialState>{
      MaterialState.pressed,
      MaterialState.hovered,
      MaterialState.focused,
    };
    if (states.any(interactiveStates.contains)) {
      return Colors.blue;
    }
    return Colors.green;
  }

  @override
  Widget build(BuildContext context) {
    showBottom = false;
    final DiceBloc diceBloc = BlocProvider.of<DiceBloc>(context);
    return Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('lib/images/back.png'), fit: BoxFit.cover)),
      child: Scaffold(
        floatingActionButtonLocation: FloatingActionButtonLocation.endTop,
        floatingActionButton: IconButton(iconSize: 45,
          icon: Icon(Icons.menu, color: Colors.white,),onPressed: (){
          if (showBottom == false) {
            showBottomSheet(
                context: context,
                builder: (context) => Container(
                  color: Colors.black,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Checkbox(
                          checkColor: Colors.orange,
                          fillColor: MaterialStateProperty.resolveWith(
                              getColor),
                          value: isAudio,
                          onChanged: (bool? value) {
                            isAudio = !isAudio;
                            if (endAnimation == true) {
                              if (reverseUp == 5) reverseUp = 1;
                              if(showBottom == true) {
                                showBottomSheet(
                                    context: context, builder: (context) => SizedBox());
                              }
                              showBottom = false;
                              diceBloc.add(DropDiceSecond(
                                  initialState: randomNumber,
                                  reverseUp: reverseUp,
                                  isAudio: isAudio,
                                  isVibrate: isVibrate));
                            }
                          }),
                      Text(
                        'Audio',
                        style: TextStyle(color: Colors.white),
                      ),
                      Checkbox(
                          checkColor: Colors.orange,
                          fillColor: MaterialStateProperty.resolveWith(
                              getColor),
                          value: isVibrate,
                          onChanged: (bool? value) {
                            isVibrate = !isVibrate;
                            if (endAnimation == true) {
                              if (reverseUp == 5) reverseUp = 1;
                              if(showBottom == true) {
                                showBottomSheet(
                                    context: context, builder: (context) => SizedBox());
                              }
                              showBottom = false;
                              diceBloc.add(DropDiceSecond(
                                  initialState: randomNumber,
                                  reverseUp: reverseUp,
                                  isAudio: isAudio,
                                  isVibrate: isVibrate));
                            }
                          }),
                      Text(
                        'Vibration',
                        style: TextStyle(color: Colors.white),
                      ),
                      SizedBox(
                        width: 7.0,
                      )
                    ],
                  ),
                ));
            showBottom = true;
          } else {
            showBottomSheet(
                context: context, builder: (context) => SizedBox());
            showBottom = false;
          }
        },),
        backgroundColor: Colors.transparent,
        body: GestureDetector(
            onVerticalDragEnd: (DragEndDetails dragEndDetails) {
              if (showBottom == false) {
                showBottomSheet(
                    context: context,
                    builder: (context) => Container(
                          color: Colors.black,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Checkbox(
                                  checkColor: Colors.orange,
                                  fillColor: MaterialStateProperty.resolveWith(
                                      getColor),
                                  value: isAudio,
                                  onChanged: (bool? value) {
                                    isAudio = !isAudio;
                                    if (endAnimation == true) {
                                      if (reverseUp == 5) reverseUp = 1;
                                      if(showBottom == true) {
                                        showBottomSheet(
                                            context: context, builder: (context) => SizedBox());
                                      }
                                      showBottom = false;
                                      diceBloc.add(DropDiceSecond(
                                          initialState: randomNumber,
                                          reverseUp: reverseUp,
                                          isAudio: isAudio,
                                          isVibrate: isVibrate));
                                    }
                                  }),
                              Text(
                                'Audio',
                                style: TextStyle(color: Colors.white),
                              ),
                              Checkbox(
                                  checkColor: Colors.orange,
                                  fillColor: MaterialStateProperty.resolveWith(
                                      getColor),
                                  value: isVibrate,
                                  onChanged: (bool? value) {
                                    isVibrate = !isVibrate;
                                    if (endAnimation == true) {
                                      if (reverseUp == 5) reverseUp = 1;
                                      if(showBottom == true) {
                                        showBottomSheet(
                                            context: context, builder: (context) => SizedBox());
                                      }
                                      showBottom = false;
                                      diceBloc.add(DropDiceSecond(
                                          initialState: randomNumber,
                                          reverseUp: reverseUp,
                                          isAudio: isAudio,
                                          isVibrate: isVibrate));
                                    }
                                  }),
                              Text(
                                'Vibration',
                                style: TextStyle(color: Colors.white),
                              ),
                              SizedBox(
                                width: 7.0,
                              )
                            ],
                          ),
                        ));
                showBottom = true;
              } else {
                showBottomSheet(
                    context: context, builder: (context) => SizedBox());
                showBottom = false;
              }
            },
            onTap: () {
              if (endAnimation == true) {
                if (reverseUp == 5) reverseUp = 1;
                if(showBottom == true) {
                  showBottomSheet(
                      context: context, builder: (context) => SizedBox());
                }
                showBottom = false;
                diceBloc.add(DropDiceSecond(
                    initialState: randomNumber,
                    reverseUp: reverseUp,
                    isAudio: isAudio,
                    isVibrate: isVibrate));
              }
            },
            child: Container(
                child: Center(
                    child: Cube(
              interactive: false,
              onSceneCreated: (Scene scene) async {
                scene.world.add(dice);
                dice.rotation.setFrom(diceApi.getRandom(initialState));
                dice.updateTransform();
                scene.camera.zoom = 2;
                scene.update();

                bool endZoomOut = false;
                if (reverseUp == 3) {
                  double i = 0;
                  for (double timeX = 0; timeX < 1000; timeX = timeX + 0.4) {
                    await Future<void>.delayed(Duration(microseconds: 50));
                    if (i < 1.55 && endZoomOut == false) {
                      i = i + 0.1;
                      dice.position.setValues(i, 0, 0);
                      dice.updateTransform();
                      scene.update();
                    } else if (i > 1.549) {
                      i = -1.55;
                      endZoomOut = true;
                    } else if (i < 0 && endZoomOut == true) {
                      i = i + 0.001;
                      dice.position.setValues(i, 0, 0);
                      dice.updateTransform();
                      scene.update();
                    }
                    dice.rotation.setValues(timeX, timeX, timeX);
                    dice.updateTransform();
                    scene.update();
                  }
                } else {
                  double i = 0;
                  for (double timeX = 0; timeX < 1200; timeX = timeX + 0.4) {
                    await Future<void>.delayed(Duration(microseconds: 50));
                    if (i < 2.55 && endZoomOut == false) {
                      i = i + 0.1;
                      dice.position.setValues(0, i, 0);
                      dice.updateTransform();
                      scene.update();
                    } else if (i > 2.549) {
                      i = -2.55;
                      endZoomOut = true;
                    } else if (i < 0 && endZoomOut == true) {
                      i = i + 0.001;
                      dice.position.setValues(0, i, 0);
                      dice.updateTransform();
                      scene.update();
                    }
                    dice.rotation.setValues(timeX, timeX, timeX);
                    dice.updateTransform();
                    scene.update();
                  }
                }
                dice.rotation.setFrom(diceApi.getRandom(randomNumber));
                dice.updateTransform();
                scene.update();
                reverseUp++;
                endAnimation = true;
              },
            )))),
      ),
    );
  }
}

class SecondDice extends StatelessWidget {
  Object dice;
  int randomNumber;
  bool endAnimation = false;
  int initialState;
  int reverseUp;
  bool isAudio;
  bool isVibrate;

  SecondDice(
      {required this.dice,
      required this.randomNumber,
      required this.initialState,
      required this.reverseUp,
      required this.isAudio,
      required this.isVibrate});

  final diceApi = DiceApi();
  bool showBottom = false;

  Color getColor(Set<MaterialState> states) {
    const Set<MaterialState> interactiveStates = <MaterialState>{
      MaterialState.pressed,
      MaterialState.hovered,
      MaterialState.focused,
    };
    if (states.any(interactiveStates.contains)) {
      return Colors.blue;
    }
    return Colors.green;
  }

  @override
  Widget build(BuildContext context) {
    final DiceBloc diceBloc = BlocProvider.of<DiceBloc>(context);
    showBottom = false;
    return Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('lib/images/back.png'), fit: BoxFit.cover)),
      child: Scaffold(
        floatingActionButtonLocation: FloatingActionButtonLocation.endTop,
        floatingActionButton: IconButton(iconSize: 45,
          icon: Icon(Icons.menu, color: Colors.white),onPressed: (){
          if (showBottom == false) {
            showBottomSheet(
                context: context,
                builder: (context) => Container(
                  color: Colors.black,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Checkbox(
                          checkColor: Colors.orange,
                          fillColor: MaterialStateProperty.resolveWith(
                              getColor),
                          value: isAudio,
                          onChanged: (bool? value) {
                            isAudio = !isAudio;
                            if (endAnimation == true) {
                              if (reverseUp == 5) reverseUp = 1;
                              if(showBottom == true) {
                                showBottomSheet(
                                    context: context, builder: (context) => SizedBox());
                              }
                              showBottom = false;
                              diceBloc.add(DropDiceFirst(
                                  initialState: randomNumber,
                                  reverseUp: reverseUp,
                                  isAudio: isAudio,
                                  isVibrate: isVibrate));
                            }
                          }),
                      Text(
                        'Audio',
                        style: TextStyle(color: Colors.white),
                      ),
                      Checkbox(
                          checkColor: Colors.orange,
                          fillColor: MaterialStateProperty.resolveWith(
                              getColor),
                          value: isVibrate,
                          onChanged: (bool? value) {
                            isVibrate = !isVibrate;
                            if (endAnimation == true) {
                              if (reverseUp == 5) reverseUp = 1;
                              if(showBottom == true) {
                                showBottomSheet(
                                    context: context, builder: (context) => SizedBox());
                              }
                              showBottom = false;
                              diceBloc.add(DropDiceFirst(
                                  initialState: randomNumber,
                                  reverseUp: reverseUp,
                                  isAudio: isAudio,
                                  isVibrate: isVibrate));
                            }
                          }),
                      Text(
                        'Vibration',
                        style: TextStyle(color: Colors.white),
                      ),
                      SizedBox(
                        width: 7.0,
                      )
                    ],
                  ),
                ));
            showBottom = true;
          } else {
            showBottomSheet(
                context: context, builder: (context) => SizedBox());
            showBottom = false;
          }
        },),
        backgroundColor: Colors.transparent,
        body: GestureDetector(
            onVerticalDragEnd: (DragEndDetails dragEndDetails) {
              if (showBottom == false) {
                showBottomSheet(
                    context: context,
                    builder: (context) => Container(
                          color: Colors.black,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Checkbox(
                                  checkColor: Colors.orange,
                                  fillColor: MaterialStateProperty.resolveWith(
                                      getColor),
                                  value: isAudio,
                                  onChanged: (bool? value) {
                                    isAudio = !isAudio;
                                    if (endAnimation == true) {
                                      if (reverseUp == 5) reverseUp = 1;
                                      if(showBottom == true) {
                                        showBottomSheet(
                                            context: context, builder: (context) => SizedBox());
                                      }
                                      showBottom = false;
                                      diceBloc.add(DropDiceFirst(
                                          initialState: randomNumber,
                                          reverseUp: reverseUp,
                                          isAudio: isAudio,
                                          isVibrate: isVibrate));
                                    }
                                  }),
                              Text(
                                'Audio',
                                style: TextStyle(color: Colors.white),
                              ),
                              Checkbox(
                                  checkColor: Colors.orange,
                                  fillColor: MaterialStateProperty.resolveWith(
                                      getColor),
                                  value: isVibrate,
                                  onChanged: (bool? value) {
                                    isVibrate = !isVibrate;
                                    if (endAnimation == true) {
                                      if (reverseUp == 5) reverseUp = 1;
                                      if(showBottom == true) {
                                        showBottomSheet(
                                            context: context, builder: (context) => SizedBox());
                                      }
                                      showBottom = false;
                                      diceBloc.add(DropDiceFirst(
                                          initialState: randomNumber,
                                          reverseUp: reverseUp,
                                          isAudio: isAudio,
                                          isVibrate: isVibrate));
                                    }
                                  }),
                              Text(
                                'Vibration',
                                style: TextStyle(color: Colors.white),
                              ),
                              SizedBox(
                                width: 7.0,
                              )
                            ],
                          ),
                        ));
                showBottom = true;
              } else {
                showBottomSheet(
                    context: context, builder: (context) => SizedBox());
                showBottom = false;
              }
            },
            onTap: () {
              if (endAnimation == true) {
                if (reverseUp == 5) reverseUp = 1;
                if(showBottom == true) {
                  showBottomSheet(
                      context: context, builder: (context) => SizedBox());
                }
                showBottom = false;
                diceBloc.add(DropDiceFirst(
                    initialState: randomNumber,
                    reverseUp: reverseUp,
                    isAudio: isAudio,
                    isVibrate: isVibrate));
              }
            },
            child: Container(
                child: Center(
                    child: Cube(
              interactive: false,
              onSceneCreated: (Scene scene) async {
                scene.world.add(dice);
                dice.rotation.setFrom(diceApi.getRandom(initialState));
                dice.updateTransform();
                scene.camera.zoom = 2;
                scene.update();

                bool endZoomOut = false;
                if (reverseUp == 4) {
                  double i = 0;
                  for (double timeX = 0; timeX < 1000; timeX = timeX + 0.4) {
                    await Future<void>.delayed(Duration(microseconds: 50));
                    if (i > -1.55 && endZoomOut == false) {
                      i = i - 0.1;
                      dice.position.setValues(i, 0, 0);
                      dice.updateTransform();
                      scene.update();
                    } else if (i < -1.5) {
                      i = 1.55;
                      endZoomOut = true;
                    } else if (i > 0 && endZoomOut == true) {
                      i = i - 0.001;
                      dice.position.setValues(i, 0, 0);
                      dice.updateTransform();
                      scene.update();
                    }
                    dice.rotation.setValues(timeX, timeX, timeX);
                    dice.updateTransform();
                    scene.update();
                  }
                } else {
                  double i = 0;
                  for (double timeX = 0; timeX < 1300; timeX = timeX + 0.4) {
                    await Future<void>.delayed(Duration(microseconds: 50));
                    if (i > -2.55 && endZoomOut == false) {
                      i = i - 0.1;
                      dice.position.setValues(0, i, 0);
                      dice.updateTransform();
                      scene.update();
                    } else if (i < -2.4) {
                      i = 2.55;
                      endZoomOut = true;
                    } else if (i > 0 && endZoomOut == true) {
                      i = i - 0.001;
                      dice.position.setValues(0, i, 0);
                      dice.updateTransform();
                      scene.update();
                    }
                    dice.rotation.setValues(timeX, timeX, timeX);
                    dice.updateTransform();
                    scene.update();
                  }
                }
                dice.rotation.setFrom(diceApi.getRandom(randomNumber));
                dice.updateTransform();
                scene.update();
                reverseUp++;
                endAnimation = true;
              },
            )))),
      ),
    );
  }
}
