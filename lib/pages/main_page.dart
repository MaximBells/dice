import 'package:dice/bloc/dice_bloc.dart';
import 'package:dice/pages/error_page.dart';
import 'package:dice/pages/onedice.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:dice/bloc/states.dart';

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DiceBloc, DiceState>(builder: (context, state) {
      if (state is FirstDiceState) {
        return FirstDice(
          dice: state.dice,
          randomNumber: state.randomNumber,
          initialState: state.initialState,
          reverseUp: state.reverseUp,
          isVibrate: state.isVibrate,
          isAudio: state.isAudio,
        );
      }
      if (state is SecondDiceState) {
        return SecondDice(
          randomNumber: state.randomNuber,
          dice: state.dice,
          initialState: state.initialState,
          reverseUp: state.reverseUp,
          isVibrate: state.isVibrate,
          isAudio: state.isAudio,
        );
      }
      if (state is ErrorState) {
        return ErrorPage();
      }
      return ErrorPage();
    });
  }
}
