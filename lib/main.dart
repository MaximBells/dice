import 'dart:math';

import 'package:dice/bloc/dice_bloc.dart';
import 'package:dice/bloc/states.dart';
import 'package:dice/pages/main_page.dart';
import 'package:dice/pages/onedice.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:dice/api/dice_api.dart';
import 'package:flutter_cube/flutter_cube.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  final diceApi = DiceApi();
  Object dice = new Object();
  final random = Random();

  @override
  Widget build(BuildContext context) {
    dice = diceApi.getCube();
    return BlocProvider<DiceBloc>(
        child: Scaffold(body: MainPage(),),
        create: (context) =>
            DiceBloc(FirstDiceState(dice: dice, randomNumber: random.nextInt(6), initialState: 1, reverseUp: 1, isAudio: false, isVibrate: false)));
  }
}
