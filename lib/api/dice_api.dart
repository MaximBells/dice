import 'package:flutter_cube/flutter_cube.dart';

class DiceApi {
  String getImage()=>'lib/images/two.png';

  String getSound()=>'sound.mp3';

  Vector3 getOne() => Vector3(270, 0, 0);

  Vector3 getTwo() => Vector3(180, 0, 0);

  Vector3 getThree() => Vector3(0, 90, 0);

  Vector3 getFour() => Vector3(0, 270, 0);

  Vector3 getFive() => Vector3(0, 0, 0);

  Vector3 getSix() => Vector3(90, 0, 0);

  Object getCube() => Object(fileName: 'lib/images/dice/dado.obj');

  Future<Object> getAnimation(Object dice) async {
    for (double timeX = 0; timeX < 1000; timeX = timeX + 0.4) {
      await Future<void>.delayed(Duration(microseconds: 100));
      dice.rotation.setValues(timeX, timeX, timeX);
      dice.updateTransform();
    }
    return dice;
  }

  Vector3 getRandom(int number) {
    switch (number) {
      case 1:
        return getOne();
      case 2:
        return getTwo();
      case 3:
        return getThree();
      case 4:
        return getFour();
      case 5:
        return getFive();
      case 6:
        return getSix();
      default:
        return getOne();
    }
  }
}
